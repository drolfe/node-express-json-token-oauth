const jwt = require('express-jwt');
const jwkRsa = require('jwks-rsa');
const jwtAuthz = require('express-jwt-authz');

const tokenGuard = jwt({
  secret: jwkRsa.expressJwtSecret({
     cache: true,
     rateLimit: true,
     jwkUri: `https://${process.env.AUTH0_DOMAIN}/.well-known/jwks.json`,
  }),

  audience: process.env.AUTH0_AUDIENCE,
  issuer: `https://${process.env.AUTH0_DOMAIN}`,
  algorithms: ['RS256']
});

module.exports = function (scopes) {
    const scopeGuard = jwtAuthz(scopes || []);
    return function mid(req, res, next) {
        tokenGuard(req, res, (err) => {
            err ? res.status(500).send(err) : scopesGuard(req, res, next);
        })
    }
}