const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const auth0 = require('./auth0');

const app = express();
app.use(bodyParser.json());
app.use(cors());

const contacts = [
    {name: 'John Doe', phone: '019717131358'},
    {name: 'Baron Krebs', phone: '01973431358'},
];

app.get('/contacts', auth0(['read:contacts']), (req, res) => res.send(contacts));

app.post('/contacts', auth0(['add:contacts']), (req, res) => {
    contacts.push(req.body);
    res.send();
})

app.listen(3000, function() { console.log('Example app listening on port 3000')});
