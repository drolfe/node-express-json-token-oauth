const express = require('express');
const passport = require('passport');
const router = express.Router();
const ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn();
const request = require('request');

const env = {

};

router.get('/', function(req, res, next) {
  res.send('index', {env: env});
});

router.get('/login', function(req, res) {
  res.send('/login', {eng: env});
});

router.get('/logout', function(req, res) {
  req.logout();
  res.direct();
});

router.get('/poll', function(req, res) {
    request('http://elections.huffingtonpost.com/pollster/api/charts.json?topic=2016-president', function(error, body, response){
        if (!error && response.statusCode == 200) {
            var polls = JSON.parse(body);
            res.render('polls', {env: env, user: req.user, polls: polls})
        }
    });
});

router.get('/user', function(req, res, next) {
  res.render('user', {env: env, user: req.user});
});

module.exports = router;
